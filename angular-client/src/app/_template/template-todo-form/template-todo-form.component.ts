import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { EventPing } from 'src/app/_interface/eventping';
import { toDoFactory, ToDo } from 'src/app/_interface/todo';

@Component({
    selector: 'app-template-todo-form',
    templateUrl: './template-todo-form.component.html',
    styleUrls: ['./template-todo-form.component.sass']
})
export class TemplateTodoFormComponent implements OnInit {

    @Output() ping: EventEmitter<any> = new EventEmitter()
    public todo$: ToDo

    constructor() {
        this.todo$ = toDoFactory()
     }

    ngOnInit(): void {
    }

    public createToDo(): void {
        const eventObject: EventPing = {
            label: 'create',
            object: this.todo$
        }
        this.todo$ = toDoFactory()
        this.ping.emit(eventObject)
    }

}
