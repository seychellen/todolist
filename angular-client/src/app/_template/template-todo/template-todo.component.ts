import { EventPing } from '../../_interface/eventping';
import { toDoFactory, ToDo } from '../../_interface/todo';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'app-template-todo',
    templateUrl: './template-todo.component.html',
    styleUrls: ['./template-todo.component.sass']
})
export class TemplateTodoComponent implements OnInit {

    @Input() todo$: ToDo
    @Output() ping: EventEmitter<any> = new EventEmitter()

    constructor() {
        this.todo$ = toDoFactory()
     }

    ngOnInit(): void {
    }

    public changeCheck(): void {
        this.todo$.status = !this.todo$.status
        const eventObject: EventPing = {
            label: 'check',
            object: this.todo$
        }
        this.ping.emit(eventObject)
    }

    public changeLabel(): void {
        const eventObject: EventPing = {
            label: 'label',
            object: this.todo$
        }
        this.ping.emit(eventObject)
    }

    public deleteToDo(): void {
        const eventObject: EventPing = {
            label: 'delete',
            object: this.todo$
        }
        this.ping.emit(eventObject)
    }
}
