import { ToDo } from 'src/app/_interface/todo';
import { EventPing } from '../_interface/eventping';
import { Component, OnInit } from '@angular/core';
import { DataService } from '../_service/data.service';

@Component({
    selector: 'app-page-list',
    templateUrl: './page-list.component.html',
    styleUrls: ['./page-list.component.sass']
})
export class PageListComponent implements OnInit {

    public toDoShow: boolean
    public doneShow: boolean
    public $todos: ToDo[];
    public $dones: ToDo[];

    constructor(private _dataService: DataService) {
        this.toDoShow = true
        this.doneShow = false
        this.$todos = []
        this.$dones = []
        this.loadData()
    }

    ngOnInit(): void {
    }

    loadData() {
        this._dataService.getToDo().subscribe((todos: ToDo[]) => {
            todos.forEach((todo: ToDo) => {
                if (todo.status) {
                    this.$dones.push(todo)
                }
                else {
                    this.$todos.push(todo)
                }
            })
        }, (error: any) => {
            console.log(`%cERROR: ${error.message}`, `color: red; font-size: 12px;`);
        });
    }


    update(event: EventPing): void {
        console.log(event)
        switch (event.label) {
            case 'check':
                this._dataService.putToDo(event.object).subscribe((data: any) => {
                    if (data.status) {
                        this.$todos = this.$todos.filter(todo => todo.id !== event.object.id)
                        this.$dones.push(data)
                    }
                    else {
                        this.$dones = this.$dones.filter(todo => todo.id !== event.object.id)
                        this.$todos.push(data)
                    }
                });
                break;

            case 'label':
                this._dataService.putToDo(event.object).subscribe((data: any) => {
                    if (event.object.status) {
                        this.$dones.forEach(done => {
                            if (done.id === data.id) {
                                done.label = data.label
                            }
                        })
                    }
                    else {
                        this.$todos.forEach(todo => {
                            if (todo.id === data.id) {
                                todo.label = data.label
                            }
                        })
                    }
                });
                break;

            case 'delete':
                this._dataService.deleteToDo(event.object).subscribe(() => {
                    if (event.object.status) {
                        this.$dones = this.$dones.filter(todo => todo.id !== event.object.id)
                    }
                    else {
                        this.$todos = this.$todos.filter(todo => todo.id !== event.object.id)
                    }
                }, (error: any) => {
                    console.log(`%cERROR: ${error.message}`, `color: red; font-size: 12px;`);
                });
                break;

            case 'create':
                event.object.position = this.$todos.length + 1

                this._dataService.postToDo(event.object).subscribe((data: ToDo) => {
                    this.$todos.push(data)
                }, (error: any) => {
                    console.log(`%cERROR: ${error.message}`, `color: red; font-size: 12px;`);
                });
                break;
        }
    }
}
