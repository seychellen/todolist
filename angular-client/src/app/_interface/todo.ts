export interface ToDo {
    id?: string;
    label?: string;
    status: boolean;
    position?: number;
}

export let toDoFactory = function () {
    return <ToDo>{
        id: undefined,
        label: "",
        status: false,
        position: undefined
    }
}
