# ToDo

## Bauen des Containers mit Push...
```
▶ mvn --settings settings.xml clean install jib:build
```

## Starten des Servers

▶ docker run -d --network mongo-network -p 8080:8080 -e DB_HOST=mongodb registry.gitlab.com/dockerschulung/joerg/todoserver:1.0

## Bauen des Clients
```
ng build --configuration production
docker build -t todoclient .
docker run -d --rm --network mongo-network -p 4200:80 todoclient
```

