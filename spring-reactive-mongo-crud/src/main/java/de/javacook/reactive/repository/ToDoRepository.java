package de.javacook.reactive.repository;

import de.javacook.reactive.dto.ToDoDto;
import de.javacook.reactive.entity.ToDo;
import org.springframework.data.domain.Range;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public interface ToDoRepository extends ReactiveMongoRepository<ToDo,String> {
}
