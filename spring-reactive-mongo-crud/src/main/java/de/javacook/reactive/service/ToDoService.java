package de.javacook.reactive.service;

import de.javacook.reactive.dto.ToDoDto;
import de.javacook.reactive.repository.ToDoRepository;
import de.javacook.reactive.utils.AppUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Range;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class ToDoService {

    @Autowired
    private ToDoRepository repository;


    public Flux<ToDoDto> getToDos(){
        return repository.findAll().map(AppUtils::entityToDto);
    }

    public Mono<ToDoDto> getToDo(String id){
        return repository.findById(id).map(AppUtils::entityToDto);
    }

    public Mono<ToDoDto> saveToDo(Mono<ToDoDto> toDoDtoMono){
        System.out.println("service method called ...");
      return  toDoDtoMono.map(AppUtils::dtoToEntity)
                .flatMap(repository::insert)
                .map(AppUtils::entityToDto);
    }

    public Mono<ToDoDto> updateToDo(Mono<ToDoDto> toDoDtoMono, String id){
       return repository.findById(id)
                .flatMap(p->toDoDtoMono.map(AppUtils::dtoToEntity)
                .doOnNext(e->e.setId(id)))
                .flatMap(repository::save)
                .map(AppUtils::entityToDto);

    }

    public Mono<Void> deleteToDo(String id){
        return repository.deleteById(id);
    }
}
