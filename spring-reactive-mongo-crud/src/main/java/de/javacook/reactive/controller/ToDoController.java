package de.javacook.reactive.controller;

import de.javacook.reactive.dto.ToDoDto;
import de.javacook.reactive.service.ToDoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/todos")
@CrossOrigin
public class ToDoController {

    @Autowired
    private ToDoService service;

    @GetMapping
    public Flux<ToDoDto> getToDos(){
        return service.getToDos();
    }

    @GetMapping("/{id}")
    public Mono<ToDoDto> getToDo(@PathVariable String id){
        return service.getToDo(id);
    }

    @PostMapping
    public Mono<ToDoDto> saveToDo(@RequestBody Mono<ToDoDto> toDoDtoMono){
        System.out.println("controller method called ...");
        return service.saveToDo(toDoDtoMono);
    }

    @PutMapping("/{id}")
    public Mono<ToDoDto> updateToDoF(@RequestBody Mono<ToDoDto> toDoDtoMono, @PathVariable String id){
        return service.updateToDo(toDoDtoMono,id);
    }

    @DeleteMapping("/{id}")
    public Mono<Void> deleteToDo(@PathVariable String id){
        return service.deleteToDo(id);
    }



}
