package de.javacook.reactive.utils;

import de.javacook.reactive.dto.ToDoDto;
import de.javacook.reactive.entity.ToDo;
import org.springframework.beans.BeanUtils;

public class AppUtils {


    public static ToDoDto entityToDto(ToDo toDo) {
        ToDoDto toDoDto = new ToDoDto();
        BeanUtils.copyProperties(toDo, toDoDto);
        return toDoDto;
    }

    public static ToDo dtoToEntity(ToDoDto toDoDto) {
        ToDo toDo = new ToDo();
        BeanUtils.copyProperties(toDoDto, toDo);
        return toDo;
    }
}
