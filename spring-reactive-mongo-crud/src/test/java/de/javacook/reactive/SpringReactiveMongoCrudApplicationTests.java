package de.javacook.reactive;

import de.javacook.reactive.controller.ToDoController;
import de.javacook.reactive.dto.ToDoDto;
import de.javacook.reactive.service.ToDoService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import  static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@WebFluxTest(ToDoController.class)
class SpringReactiveMongoCrudApplicationTests {
    @Autowired
    private WebTestClient webTestClient;
    @MockBean
    private ToDoService service;

    @Test
    public void addToDoTest(){
		Mono<ToDoDto> toDoDtoMono=Mono.just(new ToDoDto("102","mobile",1,false));
		when(service.saveToDo(toDoDtoMono)).thenReturn(toDoDtoMono);

		webTestClient.post().uri("/todos")
				.body(Mono.just(toDoDtoMono), ToDoDto.class)
				.exchange()
				.expectStatus().isOk();//200

	}


	@Test
	public void getToDosTest(){
		Flux<ToDoDto> toDoDtoFlux=Flux.just(new ToDoDto("102","mobile",1,false),
				new ToDoDto("103","TV",1,true));
		when(service.getToDos()).thenReturn(toDoDtoFlux);

		Flux<ToDoDto> responseBody = webTestClient.get().uri("/todos")
				.exchange()
				.expectStatus().isOk()
				.returnResult(ToDoDto.class)
				.getResponseBody();

		StepVerifier.create(responseBody)
				.expectSubscription()
				.expectNext(new ToDoDto("102","mobile",1,false))
				.expectNext(new ToDoDto("103","TV",1,true))
				.verifyComplete();

	}


	@Test
	public void getToDoTest(){
		Mono<ToDoDto> toDoDtoMono=Mono.just(new ToDoDto("102","mobile",1,false));
		when(service.getToDo(any())).thenReturn(toDoDtoMono);

		Flux<ToDoDto> responseBody = webTestClient.get().uri("/todos/102")
				.exchange()
				.expectStatus().isOk()
				.returnResult(ToDoDto.class)
				.getResponseBody();

		StepVerifier.create(responseBody)
				.expectSubscription()
				.expectNextMatches(p->p.getLabel().equals("mobile"))
				.verifyComplete();
	}


	@Test
	public void updateToDoTest(){
		Mono<ToDoDto> toDoDtoMono=Mono.just(new ToDoDto("102","mobile",1,false));
		when(service.updateToDo(toDoDtoMono,"102")).thenReturn(toDoDtoMono);

		webTestClient.put().uri("/todos/102")
				.body(Mono.just(toDoDtoMono), ToDoDto.class)
				.exchange()
				.expectStatus().isOk();//200
	}

	@Test
	public void deleteToDoTest(){
    	given(service.deleteToDo(any())).willReturn(Mono.empty());
		webTestClient.delete().uri("/todos/102")
				.exchange()
				.expectStatus().isOk();//200
	}

}
