#!/bin/zsh
docker network create mongo-network
docker run -d -p 27017:27017 --rm \
  -e MONGO_INITDB_ROOT_USERNAME=admin \
  -e MONGO_INITDB_ROOT_PASSWORD=password \
  --name mongodb \
  --net mongo-network \
  mongo